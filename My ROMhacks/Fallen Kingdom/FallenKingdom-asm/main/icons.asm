.orga 0xAD57EC
.headersize 0x254DBF4
.area 0x6000

icon_heart_full:
.incbin "../img/heart-full.bin"

icon_heart_half:
.incbin "../img/heart-half.bin"

icon_heart_empty:
.incbin "../img/heart-empty.bin"

icon_half_heart_full:
.incbin "../img/half-heart-full.bin"

icon_half_heart_empty:
.incbin "../img/half-heart-empty.bin"

icon_star:
.incbin "../img/icon-star.bin"

icon_boots:
.incbin "../img/icon-boots.bin"

icon_bombs:
.incbin "../img/icon-bombs.bin"

icon_lens:
.incbin "../img/icon-lens.bin"

icon_feather:
.incbin "../img/icon-feather.bin"

icon_wood:
.incbin "../img/icon-wood.bin"

icon_fire:
.incbin "../img/icon-fire.bin"

icon_earth:
.incbin "../img/icon-earth.bin"

icon_metal:
.incbin "../img/icon-metal.bin"

icon_water:
.incbin "../img/icon-water.bin"

icon_key:
.incbin "../img/key.bin"

/* Shroom Textures */
shroom_texture_eyes:
.incbin "../img/shroom-eyes.bin"

shroom_texture_red:
.incbin "../img/shroom-red.bin"

shroom_texture_green:
.incbin "../img/shroom-green.bin"

shroom_texture_yellow:
.incbin "../img/shroom-yellow.bin"

.endarea

.orga 0xA94644
.headersize 0x757E7E4
.area 0x6000

shroom_texture_pink:
.incbin "../img/shroom-pink.bin"

shroom_vertices:
.incbin "./actors/shroom-vertices.bin"

.endarea
